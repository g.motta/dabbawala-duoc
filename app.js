const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const config = require('./config');
const clima = require('./web_service/getClima').getClima;
const dolar = require('./web_service/getDolar').getDolar;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization,token,Content-Length, X-Requested-With, Accept");

    // Pass to next layer of middleware
    next();
});


app.use(require('./controller/usuario'));

app.get('/clima', async function (req, res) {

    let response = await clima('-33.459999', '-70.639999');

    res.json({
        data: response.data
    });

});

app.get('/Money', async function (req, res) {

    let response = await dolar();

    res.json({
        data: response.data
    });

});


mongoose.connect(config.mongoServer.urlMongo, { useNewUrlParser: true, useCreateIndex: true }, (err, res) => {

    if (err) throw err;

    console.log('Base de datos ONLINE');

});


app.listen(config.Server.port, function () {

    console.log(`Example app listening on port ${config.Server.port}!`);
});












