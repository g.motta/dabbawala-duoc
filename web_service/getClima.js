const axios = require('axios');
const { key } = require('./../config').Api_Key_Web_Service


//Para ver la lista de ciudades consultar el archivo city_list.json
/* Santiago 
"lat":"-33.459999"
"lon":"-70.639999"
*/
const getClima = async (lat, lng) => {

    const resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&appid=${key}&units=metric`)

    return resp;

}


module.exports = {
    getClima
}
